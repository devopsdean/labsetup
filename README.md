# my-home-lab-setup

This page will help you to setup your home lab setup with minimum configuration. 

```
CPU 4-8 cores
Memory 8GB to 32GB
Storage 20GB to 500GB
```

## Setup home lab with LXD Containers.

- Installing LXD on Ubuntu 18.04
  ```
  sudo apt-get install lxd
  sudo systemctl start lxd
  sudo systemctl status lxd
  ```
- Add your user into the LXD group to run the lxd commands
  ```
  sudo gpasswd -a <user-name> lxd
  ```
- Setup LXD - The setup will ask few questions that will design the lxd setup on your local environment.
  ```
  lxd init
  Would you like to use lxd clustring? (yes/no) [default=no]: no
  Do you want to configure a new storage pool? (yes/no) [default=yes]: yes
  Name of the new storage pool [default=default]: default
  Name of the storage backend to use (trfs, dir, lvm) [default=btrfs]: dir
  Would you like to connect to a MASS server? (yes/no) [default=no]: no
  Would you like to create a new local network bridge? (yes/no) [default=yes]: yes
  What shoud the new bridge called? [default=lxdbr0]: lxdbr0
  What IPv4 address should be used? (CIDR subnet notation, "auto" or "none") [default=auto]: auto
  What IPv6 address should be used? (CIDR subnet notation, "auto" or "none") [default=auto]: auto
  Would you like lxd to be availale over the network? (yes/no) [default=yes]: yes
  Would you like scale cached images to be updated automatically? (yes/no) [default=yes]: yes
  Would you like YMAL "lxd init" preseed to be printed? (yes/no) [default=no]: no

  lxc version

  lxc help
  ```

  If you need more help for a specific command then you can use.
  ```
  lxc help <command-name>
  ```

  You can list the location where exactily the lxc stores the machine images.

  ```
  lxc storage list
  ```

 The LXD will pull the images from the remote location, to list that you can use following command.

 ```
 lxc remote list
 ```

 To launch the lxd machine you can use the follwing command.

 ```
 lxc image list

 lxc image list images:

 lxc image list images:centos

 ```

 The lxd containers commands

 To list all running containers
 ```
 lxc list
 ```

 To launch new container, here in this command the ubuntu-16.04 image container will start.
 ```
 lxc launch ubuntu:1604 - start a new container
 ```

To check the birdge network for lxd containers

```
ip a p
```
Container mantainance commands

```
lxc stop <container-name>
lxc start <container-name>
lxc delete <container-name>
lxc delete --force <container-name>
lxc image list
lxc launch ubuntu:16.04 my-cubuntu
lxc copy my-ubuntu my-copy-ubuntu - quick way to create container
```
To move one container to another
```
lxc stop <container-name>
lxc move <container-name1> <container-name2>
```

To connect from one container to another containers
```
lxc exec <container-name> bash

```

## Configure custom profile for lxd Containers

Create a lxc profile to limit the resources used by the lxc containers

```
lxc profile list
lxc profile show default
lxc prilfe copy default myprofile
```
Edit the copied profile and create profile which we will use to launch the lxc containers

```
lxc profile edit myprofile

```
The following contents can used to create the myprofile profile

```
config:
  limits.cpu: "2"
  limits.memory: 2GB
  limits.memory.swap: "false"
  linux.kernel_modules: ip_tables,ip6_tables,netlink_diag,nf_nat,overlay
  raw.lxc: "lxc.apparmor.profile=unconfined\nlxc.cap.drop= \nlxc.cgroup.devices.allow=a\nlxc.mount.auto=proc:rw
    sys:rw"
  security.privileged: "true"
  security.nesting: "true"
description: LXD profile for Kubernetes
devices:
  eth0:
    name: eth0
    nictype: bridged
    parent: lxdbr0
    type: nic
  root:
    path: /
    pool: default
    type: disk
name: k8s
used
```
